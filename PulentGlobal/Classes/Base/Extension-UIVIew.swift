//
//  Extension-UIVIew.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
extension UIView {
    func addContraintsWithFormat(format: String, views: UIView...) {
        var viewDictionaty = [String: UIView]()

        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewDictionaty[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewDictionaty))
    }
}

private var loadingView: UIView?
extension UIViewController {
    func showLoading(onView: UIView) {
        let loadView = UIView.init(frame: onView.bounds)
        loadView.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .gray)
        ai.startAnimating()
        ai.center = loadView.center

        DispatchQueue.main.async {
            loadView.addSubview(ai)
            onView.addSubview(loadView)
        }
        loadingView = loadView
    }
    func removeLoading() {
        DispatchQueue.main.async {
            loadingView?.removeFromSuperview()
            loadingView = nil
        }
    }
}
