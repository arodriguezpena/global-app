//
//  Utils.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation
import UIKit
enum Utils {
    static func simpleAlert( _ currentView: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let btn = UIAlertAction(title: "Aceptar", style: .default)
        alert.addAction(btn)
        currentView.present(alert, animated: true)
    }
}
