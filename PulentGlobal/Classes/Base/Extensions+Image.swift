//
//  Extensions+Image.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
import UIKit
private let imageCache = NSCache<AnyObject, AnyObject>()
private let _size = CGSize(width: 100, height: 100)

private func setCache(uri: String, data: Data) {
    UserDefaults().set(data, forKey: uri)
}

private func getCache(uri: String) -> UIImage? {
    if let data = UserDefaults().data(forKey: uri) {
        return UIImage(data: data)
    } else {
      return nil
    }
}

extension UIImageView {
    func loadURL(at urlStr: String) {
        if let url = URL(string: urlStr) {
            image = nil

            if let img = getCache(uri: urlStr) {
                DispatchQueue.main.async {
                    self.image = img
                }
            }
            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: url) {
                    if let image = UIImage(data: data) {
                        DispatchQueue.main.async {
                            setCache(uri: urlStr, data: data)
                            self?.image = image
                        }
                    }
                }
            }
        }
    }
}
