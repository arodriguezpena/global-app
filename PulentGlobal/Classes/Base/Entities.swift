//
//  Entities.swift
//  FBSnapshotTestCase
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation

enum ResultRecord {
    case error
    case ok
    case noRows
}

struct CollectionResultRequest: Codable {
    let resultCount: Int
    let results: [CollectionRequest]
}
struct CollectionRequest: Codable {
    let trackId: Int64?
    let collectionId: Int64?
    let artworkUrl100: String
    let artistName: String?
    let collectionName: String?
    let previewUrl: String?
    let kind: String?
    let trackName: String?
}
