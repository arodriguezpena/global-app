//
//  ErrorView.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/4/19.
//

import Foundation
import UIKit

protocol ErrorViewControllerProtocol: class {
    func retry()
}

class ErrorView: UIView {
    weak var delegate: ErrorViewControllerProtocol?
    let containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 8
        view.backgroundColor = .white
        return view
    }()

    let titleLabel: UILabel = {
        let txt = UILabel()
        txt.text = "Ocurrio un error"
        txt.font = UIFont.boldSystemFont(ofSize: 24)
        txt.textAlignment = .center
        return txt
    }()

    let subtitleLabel: UILabel = {
        let txt = UILabel()
        txt.text = "En estos momentos no lo podemos atender."
        txt.lineBreakMode = .byWordWrapping
        txt.textAlignment = .center
        txt.numberOfLines = 0
        return txt
    }()

    let buttonRetry: UIButton = {
        let btn = UIButton()
        btn.setTitle("Reintentar", for: .normal)
        btn.backgroundColor = .gray
        btn.layer.cornerRadius = 8
        btn.addTarget(self, action: #selector(btnTap(_:)), for: .touchUpInside)
        return btn
    }()

    @objc func btnTap(_ sender: UIButton) {
        self.delegate?.retry()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupDesing()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupDesing()
    }

    func setupDesing() {
        self.addSubview(self.containerView)

        self.addContraintsWithFormat(format: "H:|-16-[v0]-16-|", views: self.containerView)
        self.addContraintsWithFormat(format: "V:[v0(200)]", views: self.containerView)

        let xConstraint = NSLayoutConstraint(item: self.containerView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: self.containerView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: -50)

        self.addConstraint(xConstraint)
        self.addConstraint(yConstraint)

        self.containerView.addSubview(self.titleLabel)
        self.containerView.addSubview(self.subtitleLabel)
        self.containerView.addSubview(self.buttonRetry)
        self.containerView.addContraintsWithFormat(format: "H:|-8-[v0]-8-|", views: self.titleLabel)
        self.containerView.addContraintsWithFormat(format: "H:|-16-[v0]-16-|", views: self.subtitleLabel)
        self.containerView.addContraintsWithFormat(format: "V:|-40-[v0]-8-[v1(50)]", views: self.titleLabel, self.subtitleLabel)

        self.containerView.addContraintsWithFormat(format: "H:|-[v0]-|", views: self.buttonRetry)
        self.containerView.addContraintsWithFormat(format: "V:[v0(50)]-|", views: self.buttonRetry)

    }
}
