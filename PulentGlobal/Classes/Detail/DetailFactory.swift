//
//  DetailFactory.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation

class DetailFactory {
    public init() {}
    public func getViewController(collection: CollectionRequest) -> UIViewController {
        let service = DetailService()
        let model = DetailModel(collection: collection)
        let presenter = DetailPresenter()
        let view = DetailViewController(presenter: presenter)

        model.service = service
        presenter.model = model
        presenter.view = view
        view.presenter = presenter
        return view
    }
}
