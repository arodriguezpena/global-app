//
//  AudioCell.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation
import UIKit
class AudioCellView: UITableViewCell {
    var item: CollectionRequest? {
        didSet {
            guard let col = item else {
                return
            }
            artWorkImage.loadURL(at: col.artworkUrl100)
            trackNameLabel.text = col.trackName
        }
    }

    let artWorkImage: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.contentMode = .scaleAspectFit
        img.layer.cornerRadius = 8
        img.clipsToBounds = true
        return img
    }()

    let trackNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Alejandro Rodriguez"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setup() {
        addSubview(artWorkImage)
        addSubview(trackNameLabel)
        self.addContraintsWithFormat(format: "H:|-10-[v0(40)]-8-[v1]-20-|", views: artWorkImage, trackNameLabel)
        self.addContraintsWithFormat(format: "V:|-10-[v0(40)]", views: artWorkImage)
        self.addContraintsWithFormat(format: "V:|-10-[v0]-10-|", views: trackNameLabel)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.artWorkImage.image = UIImage()
        self.trackNameLabel.text = ""
    }
}
