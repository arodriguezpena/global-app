//
//  DetailViewController.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation
import AVKit
protocol DetailDisplayLogic: class {
    func resultOk()
    func resultNoRows()
    func resultError()
    func resultAudioPath(path: String?)
}

class DetailViewController: UIViewController, DetailDisplayLogic {
    var presenter: DetailPresenterLogic

    @IBOutlet private weak var artworkImage: UIImageView!
    @IBOutlet private weak var collectionName: UILabel!
    @IBOutlet private weak var artistName: UILabel!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var errorView: ErrorView!

    let kCELL = "Cell"

    var selectSong: CollectionRequest?

    // MARK: - Object lifecycle
    init(presenter: DetailPresenter) {
        self.presenter = presenter
        super.init(nibName: String(describing: DetailViewController.self),
                   bundle: Bundle(for: DetailViewController.classForCoder()))
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.presenter = DetailPresenter()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.presenter = DetailPresenter()
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        loadDataSource()
        setupViews()
        self.errorView.delegate = self
    }

    func loadDataSource() {
        self.errorView.isHidden = true
        presenter.getServiceCollection()
        if let collection = presenter.getCollection() {
            artworkImage.loadURL(at: collection.artworkUrl100)
            artistName.text = collection.artistName
            collectionName.text = collection.collectionName
        }
    }

    // MARK: - Setup Views
    func setupViews() {
        tableView.register(AudioCellView.self, forCellReuseIdentifier: kCELL)
        tableView.dataSource = self
        tableView.delegate = self

    }

    // MARK: - Request
    func resultOk() {
        self.hiddenLoading()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func resultNoRows() {
        self.hiddenLoading()
    }

    func resultError() {
        self.hiddenLoading()
        self.errorView.isHidden = false
    }

    func hiddenLoading() {
        DispatchQueue.main.async {
            self.loadingIndicator.stopAnimating()
            self.loadingView.isHidden = true
        }
    }
    func showLoading() {
        loadingIndicator.startAnimating()
        loadingView.isHidden = false
    }

    func resultAudioPath(path: String?) {
        self.hiddenLoading()
        if let path = path {
            let player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerVC = AVPlayerViewController()
            playerVC.player = player
            present(playerVC, animated: true) {
                if let player = playerVC.player {
                    player.play()
                }
                if let selected = self.selectSong {
                    if let frame = playerVC.contentOverlayView?.bounds {
                        let imageView = UIImageView(image: nil)
                        imageView.loadURL(at: selected.artworkUrl100)
                        imageView.frame = frame
                        playerVC.contentOverlayView?.addSubview(imageView)
                    }
                }
            }
        } else {
            Utils.simpleAlert(self, title: "Lo sentimos", message: "El preview no esta disponible")
        }
    }
}
extension DetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getDataSourceList().count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kCELL, for: indexPath) as? AudioCellView {
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
            cell.item = presenter.getDataSourceList()[indexPath.item]
            return cell
        } else {
            fatalError("DequeueReusableCell failed while casting")
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectSong = presenter.getDataSourceList()[indexPath.item]
        if let url = selectSong?.previewUrl {
            self.showLoading()
            presenter.getSongWhitURL(url: url)
        } else {
            Utils.simpleAlert(self, title: "Lo sentimos", message: "El preview no esta disponible")
        }
    }
}
extension DetailViewController: ErrorViewControllerProtocol {
    func retry() {
        self.loadingView.isHidden = false
        loadDataSource()
    }
}
