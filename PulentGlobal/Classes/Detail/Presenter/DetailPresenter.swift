//
//  DetailPresenter.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation

protocol DetailPresenterLogic {
    func getServiceCollection()
    func getDataSourceList() -> [CollectionRequest]
    func getCollection() -> CollectionRequest?
    func getSongWhitURL(url: String)

}

class DetailPresenter: DetailPresenterLogic {
    weak var view: DetailDisplayLogic?
    var model: DetailModelLogic?

    func getServiceCollection() {
        guard let model = self.model else {
            return
        }

        model.getServiceDetailList { [weak self] result in
            guard let self = self, let view = self.view else {
                return
            }
            switch result {
            case .ok:
                view.resultOk()
            case .noRows:
                view.resultNoRows()
            case .error:
                view.resultError()
            }
        }
    }

    func getDataSourceList() -> [CollectionRequest] {
        guard let model = self.model else {
            return []
        }
        return model.getDataSourceList()
    }

    func getCollection() -> CollectionRequest? {
        guard let model = self.model else {
            return nil
        }
        return model.getCollection()
    }
    func getSongWhitURL(url: String) {
        guard let model = self.model else {
            return
        }
        model.getSongWhitURL(url: url) { [weak self] path in
            guard let self = self, let view = self.view else {
                return
            }
            view.resultAudioPath(path: path)
        }
    }
}
