//
//  DetailServices.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation
import Alamofire

protocol DetailServiceLogic {
    func getDetailResultRequest(collectionId: Int64, completion: @escaping (CollectionResultRequest?) -> Void)
    func getSongWhitURL(url: String, completion: @escaping (String?) -> Void )
}

class DetailService: DetailServiceLogic {
    func getDetailResultRequest(collectionId: Int64, completion: @escaping (CollectionResultRequest?) -> Void) {

        let urlString = "\(DetailConstants.API.getCollection)\(collectionId)&entity=song"

        if let data = self.getCache(uri: urlString) {
            completion(self.decoder(uri: urlString, data: data))
            return
        }

        AF.request(urlString).response { [weak self] response in
            guard let self = self else {
                return
            }
            guard let data = response.data else {
                completion(nil)
                return
            }
            completion(self.decoder(uri: urlString, data: data))
        }
    }

    func decoder(uri: String, data: Data) -> CollectionResultRequest? {
        do {
            let decoder = JSONDecoder()
            let collectionResultRequest = try decoder.decode(CollectionResultRequest.self, from: data)
            self.setCache(uri: uri, data: data)
            return collectionResultRequest
        } catch let error {
            print(error)
            return nil
        }
    }

    private func setCache(uri: String, data: Data) {
        let cache = UserDefaults()
        cache.set(data, forKey: uri)
    }

    private func getCache(uri: String) -> Data? {
        return UserDefaults().data(forKey: uri)
    }

    func getSongWhitURL(url: String, completion: @escaping (String?) -> Void) {

        if let path = self.getCacheSong(url: url) {
            completion(path)
            return
        }
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        AF.download(url, to: destination)
            .validate()
            .responseData(queue: DispatchQueue.global(qos: .utility)) { [weak self] response in
                guard let self = self else {
                    completion(nil)
                    return
                }
                if let path = response.fileURL?.path {
                    self.setCacheSong(url: url, pathURL: path)
                    completion(path)
                } else {
                    completion(nil)
                }
        }
    }
    private func setCacheSong(url: String, pathURL: String) {
        UserDefaults().set(pathURL, forKey: url)
    }
    private func getCacheSong(url: String) -> String? {
        return UserDefaults().string(forKey: url)
    }
}
