//
//  DetailModel.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/3/19.
//

import Foundation

protocol DetailModelLogic {
    func getServiceDetailList(completion: @escaping (ResultRecord) -> Void)
    func getDataSourceList() -> [CollectionRequest]
    func getCollection() -> CollectionRequest
    func getSongWhitURL(url: String, completion: @escaping (String?) -> Void )
}

protocol DetailModelDataStore {
    var collection: CollectionRequest { get set }
    var list: [CollectionRequest] { get set }
}

class DetailModel: DetailModelLogic, DetailModelDataStore {
    var service: DetailServiceLogic?
    var collection: CollectionRequest
    var list: [CollectionRequest] = []

    init(collection: CollectionRequest) {
        self.collection = collection
    }

    func getServiceDetailList(completion: @escaping (ResultRecord) -> Void) {
        guard let service = self.service else {
            return completion(.error)
        }
        return service.getDetailResultRequest(collectionId: self.collection.collectionId ?? 0, completion: { [weak self] result in
            guard let self = self else {
                return
            }

            if result == nil {
                completion(.error)
            } else {
                if let result = result?.results {
                    self.list = result.filter({ collection -> Bool in
                        if let kind = collection.kind {
                            return kind == "song"
                        } else {
                            return false
                        }
                    })
                    if self.list.isEmpty {
                        completion(.noRows)
                    } else {
                        completion(.ok)
                    }
                }
            }
        })
    }

    func getDataSourceList() -> [CollectionRequest] {
        return self.list
    }
    func getCollection() -> CollectionRequest {
        return self.collection
    }

    func getSongWhitURL(url: String, completion: @escaping (String?) -> Void) {
        guard let service = self.service else {
            return completion(nil)
        }
        service.getSongWhitURL(url: url, completion: { str in
             completion(str)
        })
    }
}
