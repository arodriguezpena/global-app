//
//  FinderPresenter.swift
//  FBSnapshotTestCase
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation

protocol FinderPresenterLogic {
    func getServiceCollection(term: String)
    func getDataSourceList() -> [CollectionRequest]
}
class FinderPresenter: FinderPresenterLogic {
    weak var view: FinderDisplayLogic?
    var model: FinderModelLogic?

    func getServiceCollection(term: String) {
        guard let model = self.model else {
            return
        }
        model.getServiceCollectionList(term: term) { [weak self] result in
            guard let self = self, let view = self.view else {
                return
            }
            switch result {
            case .ok:
                view.resultOk()
            case .noRows:
                view.resultNoRows()
            case .error:
                view.resultError()
            }
        }
    }

    func getDataSourceList() -> [CollectionRequest] {
        guard let model = self.model else {
            return []
        }
        return model.getDataSourceList()
    }
}
