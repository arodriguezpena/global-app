//
//  FinderServiceStub.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
import OHHTTPStubs
class FinderServiceStubs: NSObject {
    func get_200() {
        let getdFile = "GET_200_ROWS.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }
    func get_200_ROW0() {
        let getdFile = "GET_200_NOROW.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 200,
                headers: [:]
            )
        }
    }

    func get_500_ERROR() {
        let getdFile = "GET_ERROR.json"
        let path = ".*/"
        stub(condition: pathMatches(path)) { _ in
            return fixture(
                filePath: OHPathForFile(getdFile, type(of: self))! ,
                status: 500,
                headers: [:]
            )
        }
    }
}
