//
//  FinderService.swift
//  FBSnapshotTestCase
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
import Alamofire

protocol FinderServiceLogic {
    func getCollectionResultRequest(term: String, completion: @escaping (CollectionResultRequest?) -> Void)
}

class FinderService: FinderServiceLogic {
    func getCollectionResultRequest(term: String, completion: @escaping (CollectionResultRequest?) -> Void) {

        guard let urlString = "\(FinderConstants.API.getCollection)?term=\(term)&mediaType=music&limit=20".addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) else {
            completion(nil)
            return
        }

        if let data = self.getCache(uri: urlString) {
            completion(self.decoder(uri: urlString, data: data))
            return
        }

        AF.request(urlString).response { [weak self] response in
            guard let data = response.data, let self = self else {
                completion(nil)
                return
            }
            completion(self.decoder(uri: urlString, data: data))
        }
    }

    func decoder(uri: String, data: Data) -> CollectionResultRequest? {
        do {
            let decoder = JSONDecoder()
            let collectionResultRequest = try decoder.decode(CollectionResultRequest.self, from: data)
            self.setCache(uri: uri, data: data)
            return collectionResultRequest
        } catch let error {
            print(error)
           return nil
        }
    }

    func setCache(uri: String, data: Data) {
        let cache = UserDefaults()
        cache.set(data, forKey: uri)
    }

    func getCache(uri: String) -> Data? {
        return UserDefaults().data(forKey: uri)
    }
}
