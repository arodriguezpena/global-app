//
//  FinderConstants.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
public enum FinderConstants {
    public enum API {
        public static let getCollection = "https://itunes.apple.com/search"
    }
    public enum Messages {
        public static let title = "List of Collections"
        public static let search = "Search terms.."
        public static let titleNoRows = "Lo sentimos"
        public static let noRows = "No se encontraron registros"
    }
}
