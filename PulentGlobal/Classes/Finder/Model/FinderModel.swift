//
//  FinderModel.swift
//  FBSnapshotTestCase
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation

protocol FinderModelLogic {
    func getServiceCollectionList(term: String, completion: @escaping (ResultRecord) -> Void)
    func getDataSourceList() -> [CollectionRequest]
}

protocol FinderModelDataStore {
    var list: [CollectionRequest] { get set }
}

class FinderModel: FinderModelLogic, FinderModelDataStore {
    var service: FinderServiceLogic?
    var list: [CollectionRequest] = []

    func getServiceCollectionList(term: String, completion: @escaping (ResultRecord) -> Void) {
        guard let service = self.service else {
            return completion(.error)
        }
        return service.getCollectionResultRequest(term: term, completion: { [weak self] result in
            guard let self = self else {
                return
            }

            if result == nil {
                completion(.error)
            } else {
                if let result = result?.results {
                    self.list = result
                    if self.list.isEmpty {
                        completion(.noRows)
                    } else {
                        completion(.ok)
                    }
                }
            }
        })
    }

    func getDataSourceList() -> [CollectionRequest] {
        return self.list
    }

    func removeCache() {
        let defaults = UserDefaults()
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
}
