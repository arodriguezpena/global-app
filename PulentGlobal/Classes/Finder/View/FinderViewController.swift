//
//  FinderViewController.swift
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
import UIKit
protocol FinderDisplayLogic: class {
    func resultOk()
    func resultNoRows()
    func resultError()
}

class FinderViewController: UIViewController, FinderDisplayLogic {
    var presenter: FinderPresenterLogic
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var errorViewConstraint: NSLayoutConstraint!
    @IBOutlet private weak var errorView: ErrorView!

    let kCELL = "Cell"
    let searchBar = UISearchBar()

    // MARK: - Object lifecycle
    init(presenter: FinderPresenter) {
        self.presenter = presenter
        super.init(nibName: String(describing: FinderViewController.self),
                   bundle: Bundle(for: FinderViewController.classForCoder()))
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.presenter = FinderPresenter()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        self.presenter = FinderPresenter()
        super.init(coder: aDecoder)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        errorViewConfig()
    }

    // MARK: - Setup Views
    func setupViews() {
        navigationItem.title = FinderConstants.Messages.title
        errorView.delegate = self
        tableView.register(CellView.self, forCellReuseIdentifier: kCELL)
        tableView.dataSource = self
        tableView.delegate = self

        searchBar.frame = CGRect(x: 0, y: 0, width: 200, height: 70)
        searchBar.delegate = self
        searchBar.searchBarStyle = .default
        searchBar.placeholder = FinderConstants.Messages.search
        searchBar.sizeToFit()

        tableView.tableHeaderView = searchBar
    }

    func errorViewConfig() {
        let const = (self.navigationController?.navigationBar.frame.size.height ?? 0) + 75
        errorViewConstraint.constant = const
    }

    // MARK: - Request
    func resultOk() {
        self.removeLoading()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func resultNoRows() {
        self.errorView.isHidden = true
        self.removeLoading()
        let alert = UIAlertController(title: FinderConstants.Messages.titleNoRows, message: FinderConstants.Messages.noRows, preferredStyle: .alert)
        let btn = UIAlertAction(title: "Aceptar", style: .default, handler: nil)
        alert.addAction(btn)
        self.present(alert, animated: true)
        tableView.reloadData()
    }

    func resultError() {
        self.removeLoading()
        self.errorView.isHidden = false
    }

}

extension FinderViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let txt = searchBar.text else {
            return
        }
        self.errorView.isHidden = true
        self.showLoading(onView: self.view)
        self.presenter.getServiceCollection(term: txt)
        self.view.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.view.endEditing(true)
        }
    }
}
extension FinderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getDataSourceList().count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kCELL, for: indexPath) as? CellView {
            cell.selectionStyle = .none
            cell.accessoryType = .disclosureIndicator
            cell.item = presenter.getDataSourceList()[indexPath.item]
            return cell
        } else {
             fatalError("DequeueReusableCell failed while casting")
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.item
        let item = self.presenter.getDataSourceList()[index]
        let viewController = DetailFactory().getViewController(collection: item)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
extension FinderViewController: ErrorViewControllerProtocol {
    func retry() {
        if let txt = searchBar.text, !txt.isEmpty {
            self.showLoading(onView: self.view)
            self.presenter.getServiceCollection(term: txt )
            self.errorView.isHidden = true
        }
    }
}
