//  FinderCell.swift
//  Alamofire
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
import UIKit

class CellView: UITableViewCell {
    var item: CollectionRequest? {
        didSet {
            guard let col = item else {
                return
            }
            artWorkImage.loadURL(at: col.artworkUrl100)
            artistLabel.text = col.artistName
            collectionLabel.text = col.collectionName
        }
    }

    let artWorkImage: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
        img.contentMode = .scaleAspectFit
        img.layer.cornerRadius = 8
        img.clipsToBounds = true
        return img
    }()

    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()

    let artistLabel: UILabel = {
        let label = UILabel()
        label.text = "Alejandro Rodriguez"
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 18)
        return label
    }()

    let collectionLabel: UILabel = {
        let label = UILabel()
        label.text = "Coleccion número 2"
        label.font = UIFont.systemFont(ofSize: 13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func setup() {
        addSubview(artWorkImage)
        addSubview(containerView)

        self.addContraintsWithFormat(format: "H:|-10-[v0(60)]-8-[v1]-20-|", views: artWorkImage, containerView)
        self.addContraintsWithFormat(format: "V:|-10-[v0(60)]", views: artWorkImage)
        self.addContraintsWithFormat(format: "V:|-10-[v0]-10-|", views: containerView)

        containerView.addSubview(artistLabel)
        containerView.addSubview(collectionLabel)

        containerView.addContraintsWithFormat(format: "H:|-0-[v0]-0-|", views: artistLabel)
        containerView.addContraintsWithFormat(format: "H:|-0-[v0]-0-|", views: collectionLabel)
        containerView.addContraintsWithFormat(format: "V:|-0-[v0]-0-[v1]-3-|", views: artistLabel, collectionLabel)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.artWorkImage.image = UIImage()
        self.artistLabel.text = ""
        self.collectionLabel.text = ""
    }
}
