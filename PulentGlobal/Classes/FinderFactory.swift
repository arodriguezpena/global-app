//
//  FinderFactory.swift
//  FBSnapshotTestCase
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import Foundation
import UIKit

public class FinderFactory {
    public init() {}
    public func getViewController() -> UIViewController {
        let service = FinderService()
        let model = FinderModel()
        let presenter = FinderPresenter()

        let view = FinderViewController(presenter: presenter)

        model.service = service
        presenter.model = model
        presenter.view = view
        view.presenter = presenter
        return view
    }
}
