//
//  FinderUnitTest.swift
//  OHHTTPStubs
//
//  Created by Alejandro  Rodriguez on 11/2/19.
//

import XCTest
@testable import PulentGlobal
class FinderUnitTest: XCTestCase {
    let model = FinderModel()
    let presenter = FinderPresenter()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        model.service = FinderService()
        presenter.model = model
    }

    // MARK: - MODEL
    func test_get_collection_list_200() {
        model.removeCache()
        FinderServiceStubs().get_200()

        let expectation = self.expectation(description: "get Collection list")
        model.getServiceCollectionList(term: "in+utero") { result in
            XCTAssert(result == .ok)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 100, handler: nil)
        XCTAssert(model.getDataSourceList().count == 20)
    }

    func test_get_collection_list_200_cache() {
        FinderServiceStubs().get_200()

        let expectation = self.expectation(description: "get Collection list")
        model.getServiceCollectionList(term: "in+utero") { result in
            XCTAssert(result == .ok)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 100, handler: nil)
        XCTAssert(model.getDataSourceList().count == 20)
    }

    func test_get_collection_list_200_NOROW() {
        model.removeCache()
        FinderServiceStubs().get_200_ROW0()

        let expectation = self.expectation(description: "get Collection list")
        model.getServiceCollectionList(term: "iron maiden") { result in
            XCTAssert(result == .noRows)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 100, handler: nil)
    }

    func test_get_collection_list_500_ERROR() {
        model.removeCache()
        FinderServiceStubs().get_500_ERROR()

        let expectation = self.expectation(description: "get ERROR json Collection list")
        model.getServiceCollectionList(term: "megadeth") { result in
            XCTAssert(result == .error)
            expectation.fulfill()
        }

        waitForExpectations(timeout: 100, handler: nil)
    }

    // MARK: - PRESENTER
    var expectationPresenter: XCTestExpectation?
    func test_getServiceCollection_OK() {
        model.removeCache()
        FinderServiceStubs().get_200()
        self.presenter.view = self

        self.expectationPresenter = XCTestExpectation(description: "presenter")
        presenter.getServiceCollection(term: "megadeth")

        wait(for: [expectationPresenter!], timeout: 100)
        XCTAssert(presenter.getDataSourceList().count == 20)
    }

    func test_getServiceCollection_NOROW() {
        model.removeCache()
        FinderServiceStubs().get_200_ROW0()
        self.presenter.view = self

        self.expectationPresenter = XCTestExpectation(description: "presenter")
        presenter.getServiceCollection(term: "megadeth")

        wait(for: [expectationPresenter!], timeout: 100)
        XCTAssert(presenter.getDataSourceList().isEmpty)
        XCTAssert(expectationPresenter!.expectationDescription == "NOROWS")
    }

    func test_getServiceCollection_ERROR() {
        model.removeCache()
        FinderServiceStubs().get_500_ERROR()
        self.presenter.view = self

        self.expectationPresenter = XCTestExpectation(description: "presenter")
        presenter.getServiceCollection(term: "megadeth")

        wait(for: [expectationPresenter!], timeout: 100)
        XCTAssert(expectationPresenter!.expectationDescription == "ERROR")
    }
}

extension FinderUnitTest: FinderDisplayLogic {
    func resultOk() {
        expectationPresenter?.fulfill()
    }

    func resultNoRows() {
        expectationPresenter?.expectationDescription = "NOROWS"
        expectationPresenter?.fulfill()
    }

    func resultError() {
        expectationPresenter?.expectationDescription = "ERROR"
        expectationPresenter?.fulfill()
    }
}
