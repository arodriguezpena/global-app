# PulentGlobal

[![CI Status](https://img.shields.io/travis/arodriguezp2003/PulentGlobal.svg?style=flat)](https://travis-ci.org/arodriguezp2003/PulentGlobal)
[![Version](https://img.shields.io/cocoapods/v/PulentGlobal.svg?style=flat)](https://cocoapods.org/pods/PulentGlobal)
[![License](https://img.shields.io/cocoapods/l/PulentGlobal.svg?style=flat)](https://cocoapods.org/pods/PulentGlobal)
[![Platform](https://img.shields.io/cocoapods/p/PulentGlobal.svg?style=flat)](https://cocoapods.org/pods/PulentGlobal)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PulentGlobal is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PulentGlobal'
```

## Author

arodriguezp2003, alejandrorodriguezpena@gmail.com

## License

PulentGlobal is available under the MIT license. See the LICENSE file for more info.
