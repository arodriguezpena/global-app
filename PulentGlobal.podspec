#
# Be sure to run `pod lib lint PulentGlobal.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PulentGlobal'
  s.version          = '1.0.2'
  s.summary          = 'GlobalLogic test.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/arodriguezpena/global-app'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'arodriguezp2003' => 'alejandrorodriguezpena@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/arodriguezpena/global-app.git', :tag => s.version.to_s }
  s.static_framework = true
  s.swift_version = '4.2'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.2' }
  s.ios.deployment_target = '11.3'

  s.source_files = 'PulentGlobal/Classes/**/*'
  s.resources = 'PulentGlobal/Assets/**/*.{storyboard,strings,xib,xcassets,pdf,otf,ttf,plist,imageset,png}'
 
  s.test_spec 'PulentGlobal_Tests' do |test_spec|
      test_spec.source_files = 'PulentGlobal/Test/**/*'
      test_spec.resources = 'PulentGlobal/Assets/**/*.{json}'
      test_spec.dependency 'OHHTTPStubs/Swift','~> 6.1.0'
     
  end
  s.dependency 'Alamofire','~> 5.0.0-rc.3'
  s.dependency 'OHHTTPStubs/Swift','~> 6.1.0'
end
