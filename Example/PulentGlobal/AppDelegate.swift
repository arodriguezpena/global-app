//
//  AppDelegate.swift
//  PulentGlobal
//
//  Created by arodriguezp2003 on 11/02/2019.
//  Copyright (c) 2019 arodriguezp2003. All rights reserved.
//

import UIKit
import PulentGlobal
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = UINavigationController.init(rootViewController: FinderFactory().getViewController())
        self.window?.makeKeyAndVisible()
        return true
    }
}
